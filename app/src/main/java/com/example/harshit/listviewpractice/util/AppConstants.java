package com.example.harshit.listviewpractice.util;

/**
 * Created by Harshit on 1/26/2015.
 */
public interface AppConstants {
    int ADD_REQUEST_CODE = 2;
    int EDIT_REQUEST_CODE = 200;
    int POSITION_0 = 0;
    int POSITION_1 = 1;
    int POSITION_2 = 2;

}
