package com.example.harshit.listviewpractice.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.harshit.listviewpractice.R;

import static com.example.harshit.listviewpractice.main.MainActivity.rollNumbers;

public class AddStudent extends Activity {
    EditText userName;
    EditText userRollNo;
    Button save_button;
    Button cancel_button;
    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addstudent);
        userName = (EditText) findViewById(R.id.userName);
        userName.setHint("Enter a name"); //sets the hint text
        userRollNo = (EditText) findViewById(R.id.userRollNo);
        userRollNo.setHint("Enter a Roll Number"); //sets the hint text
        save_button = (Button) findViewById(R.id.save_button);
        cancel_button = (Button) findViewById(R.id.cancel_button);

        extras = getIntent().getExtras(); // Extract the bundle into extras
        if (extras != null) {

            if (extras.get("Button").equals("view")) { //If view option is selected
                String name = (String) extras.get("Name");
                userName.setText(name);//Sets the user name field to the name of the particular student
                String rollNo = (String) extras.get("RollNo");
                userRollNo.setText(rollNo);//Sets the roll number field to the roll no. of the particular student
                userName.setEnabled(false);//This is meant to keep name and
                userRollNo.setEnabled(false);// roll number fields non editable
                save_button.setVisibility(View.INVISIBLE);
                cancel_button.setText("BACK");
            }
            if (extras.get("Button").equals("edit")) { // If edit option is selected
                String name1 = (String) extras.get("Name");
                userName.setHint(name1);//Sets the hint text
                String rollNo1 = (String) extras.get("RollNo");
                userRollNo.setHint(rollNo1);
                save_button.setText("UPDATE");
            }
        }
        save_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                String name = userName.getText().toString();
                String rollNo = userRollNo.getText().toString();
                if (name.equals("") || rollNo.equals("")) {
                    Toast.makeText(AddStudent.this, "Each field needs to be filled!", Toast.LENGTH_LONG).show();
                } else if (rollNumbers.contains(rollNo)) {
                    Toast.makeText(AddStudent.this, "A student with that roll number already exists!", Toast.LENGTH_LONG).show();
                }  else if (Integer.parseInt(rollNo)==0) {
                    Toast.makeText(AddStudent.this, "Roll Number cannot be Zero!", Toast.LENGTH_LONG).show();
                } else if(name.indexOf(" ")==0){
                    Toast.makeText(AddStudent.this, "Avoid any Leading Spaces!", Toast.LENGTH_LONG).show();
                }
                else {
                    Intent intent = new Intent();
                    intent.putExtra("Name", name.trim());
                    intent.putExtra("RollNo", rollNo);
                    if (extras != null && extras.get("Button").equals("edit")) {// If it is the edit case
                        intent.putExtra("pos", extras.getInt("position"));// Pass the position also along with updated data
                    }
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
        cancel_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
