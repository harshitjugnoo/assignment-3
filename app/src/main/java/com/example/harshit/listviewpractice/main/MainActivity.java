package com.example.harshit.listviewpractice.main;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.harshit.listviewpractice.R;
import com.example.harshit.listviewpractice.entities.Student;
import com.example.harshit.listviewpractice.util.AddStudent;
import com.example.harshit.listviewpractice.util.AppConstants;
import com.example.harshit.listviewpractice.util.MyCustomAdaptor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class MainActivity extends Activity implements AppConstants {
    static ListView listview;
    static GridView gridView;
    static List<Student> list = new ArrayList<>(); //List for maintaining the Student class objects
    public static List<String> rollNumbers = new ArrayList<>(); // List for roll numbers so that they can be checked for uniqueness
    MyCustomAdaptor adaptor;
    Button addButton;
    Spinner spinner;
    Button optionListView;
    Button optionGridView;
    final Context context = this;
    Button viewOption;
    Button editOption;
    Button deleteOption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        optionGridView = (Button) findViewById(R.id.optionGridView);
        optionListView = (Button) findViewById(R.id.optionListView);
        listview = (ListView) findViewById(R.id.listView);
        gridView = (GridView) findViewById(R.id.gridView);
        adaptor = new MyCustomAdaptor(this, list);
        gridView.setAdapter(adaptor);
        listview.setAdapter(adaptor);
        addItemsOnSpinner(); //For adding items on the spinner
        addListenerOnSpinnerItemSelection(); //For performing necessary action on item selection
        optionGridView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) { //setting the grid view visible
                gridView.setVisibility(View.VISIBLE);
                listview.setVisibility(View.INVISIBLE);
            }
        });
        optionListView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) { //setting the list view visible
                listview.setVisibility(View.VISIBLE);
                gridView.setVisibility(View.INVISIBLE);

            }
        });

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                itemClick(parent, view, position, id); //function to display the dialog box
            }
        });
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                itemClick(parent, view, position, id);//function to display the dialog box
            }
        });
        addButton = (Button) findViewById(R.id.addButton);
        addButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(MainActivity.this, AddStudent.class);//Intent for moving from Main Activity to Add Student activity
                startActivityForResult(intent, ADD_REQUEST_CODE);// Activity is started with requestCode ADD_REQUEST_CODE
            }
        });
    }

    public void itemClick(AdapterView<?> parent, View view, final int position, long id) { //Displays a dialog on a list-item click
        final Dialog dialog = new Dialog(context); //create the Dialog class object
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle("Choose an option..."); //Sets the title of the dialog
        viewOption = (Button) dialog.findViewById(R.id.viewOption);
        editOption = (Button) dialog.findViewById(R.id.editOption);
        deleteOption = (Button) dialog.findViewById(R.id.deleteOption);
        viewOption.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) { //for viewing the student
                Intent view = new Intent(MainActivity.this, AddStudent.class);
                String name = list.get(position).getName();
                String rollNo = list.get(position).getRollNo();
                view.putExtra("Name", name);
                view.putExtra("RollNo", rollNo);
                view.putExtra("Button", "view"); //Bundle up the button along with name and roll number
                startActivity(view);
                dialog.dismiss();
            }
        });
        editOption.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) { //for editing the details of the student
                Intent edit = new Intent(MainActivity.this, AddStudent.class);
                String name = list.get(position).getName();
                String rollNo = list.get(position).getRollNo();
                edit.putExtra("Name", name);
                edit.putExtra("RollNo", rollNo);
                edit.putExtra("Button", "edit");
                edit.putExtra("position", position); //position is also passed
                startActivityForResult(edit, EDIT_REQUEST_CODE);
                dialog.dismiss();
            }
        });
        deleteOption.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) { //for deleting the student
                list.remove(position);
                adaptor.notifyDataSetChanged();
                dialog.dismiss();
                spinner.setSelection(POSITION_0); //sets the spinner to its default selection
            }
        });
        dialog.show();
        setFinishOnTouchOutside(true);
    }

    private void addItemsOnSpinner() { //Adds items on the spinner at run-time
        spinner = (Spinner) findViewById(R.id.spinner);
        List<String> spinnerItems = new ArrayList<>();
        spinnerItems.add("Select an option"); //Initial dummy entry
        spinnerItems.add("Sort by Name");
        spinnerItems.add("Sort by Roll Number");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, spinnerItems) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;
                // If this is the initial dummy entry, make it hidden
                if (position == POSITION_0) {
                    TextView selectOption = new TextView(getContext());
                    selectOption.setHeight(0);
                    selectOption.setVisibility(View.GONE);
                    v = selectOption;
                } else {
                    // Pass convertView as null to prevent reuse of special case views
                    v = super.getDropDownView(position, null, parent);
                }
                // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                parent.setVerticalScrollBarEnabled(false);
                return v;
            }
        };
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

    }

    private void addListenerOnSpinnerItemSelection() {
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == POSITION_1) { //Sorts on the basis of names
                    Collections.sort(list, new StudentNameComp());//Custom Comparator is defined below
                    adaptor.notifyDataSetChanged();
                } else if (position == POSITION_2) {//Sorts on the basis of roll numbers
                    Collections.sort(list, new StudentRollNoComp()); //Custom Comparator is defined below
                    adaptor.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
    }

    class StudentNameComp implements Comparator<Student> { //Custom comparator for sorting students on the basis of names

        @Override
        public int compare(Student lhs, Student rhs) {
            return lhs.getName().compareToIgnoreCase(rhs.getName());
        }
    }

    class StudentRollNoComp implements Comparator<Student> { //Custom comparator for sorting students on the basis of roll numbers

        @Override
        public int compare(Student lhs, Student rhs) {
            if(Integer.parseInt(lhs.getRollNo())<Integer.parseInt(rhs.getRollNo())){
                return -1;
            }else{
                return 1;
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_REQUEST_CODE) { // check if the request code is same as what is passed.
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra("Name");
                String rollNumber = data.getStringExtra("RollNo");
                adaptor.list.add(new Student(name, rollNumber)); //Adds the student to the list and the adapter
                adaptor.notifyDataSetChanged();
                rollNumbers.add(rollNumber); //Roll numbers are added separately in a new list for checking uniqueness
                spinner.setSelection(POSITION_0); //Sets the spinner to the default selection
            }
        } else if (requestCode == EDIT_REQUEST_CODE) { // check if the request code is same as what is passed.
            if (resultCode == RESULT_OK) {
                String name = data.getStringExtra("Name"); //Fetches the edited data
                String rollNumber = data.getStringExtra("RollNo");
                int pos = data.getIntExtra("pos", list.size()); // Also we need the position of the student which is edited
                adaptor.list.set(pos, new Student(name, rollNumber)); ////Sets the student to the list and the adapter
                adaptor.notifyDataSetChanged();
                rollNumbers.set(pos, rollNumber);//Roll numbers are added separately in a new list for checking uniqueness
                spinner.setSelection(POSITION_0);//Sets the spinner to the default selection
            }
        }
    }
}


