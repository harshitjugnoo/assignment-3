package com.example.harshit.listviewpractice.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.harshit.listviewpractice.R;
import com.example.harshit.listviewpractice.entities.Student;

import java.util.List;

/**
 * Created by Harshit on 1/21/2015.
 */
public class MyCustomAdaptor extends BaseAdapter{
    public List<Student> list;
    Context ctx;

    public MyCustomAdaptor(Context ctx,List<Student> list){
        this.ctx = ctx;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.single_row,null);
        TextView viewName = (TextView)view.findViewById(R.id.name);
        TextView viewRollNo = (TextView)view.findViewById(R.id.rollNo);
        viewName.setText(list.get(position).getName());
        viewRollNo.setText(list.get(position).getRollNo());
        return view;
    }
}
